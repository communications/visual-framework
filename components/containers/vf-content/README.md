# vf-content Pattern

Use this container to get simple support for narrative content where it is not
practical to assign classes, such as Markdown or WYSIWYG text.

This container adds basic support for `p`, `ul`, `hr`, `a` and other core
html elements. Some patterns may also add specific support for `.vf-content`
