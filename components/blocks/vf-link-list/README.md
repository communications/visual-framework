# Links List Pattern

The Links List pattern is a robust list group that can be used in a variety of grid layouts.

It can have a title `<h3 class="vf-links__heading">Example Title</h3>`.

In each list item you can have:

A link `<a class="vf-links__link" href="">Example Link</a>`.

It can make use of the Tags pattern `<span class="vf-tags">Example Tag</span>`.

It can include meta information `<p class="vf-links__meta">Example Meta</p>`.
