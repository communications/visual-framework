---
title: JavaScript guidelines
---

## Using the Visual Framework JavaScript

JavaScript in the Visual Framework is more for demonstration purposes, and isn't considered company-wide production ready. This is because of the various existing JavaScript (JS) Frameworks in place that may conflict with the code written for these patterns in this library.

You may still use the JavaScript that is included for these patterns but if it does conflict with your JS Frameworks code you will have to create any necessary new code or edits in your project codebase to fix.
